<?php
namespace backend\models;

use common\models\User;
use common\models\UserProfile;
use yii\base\Exception;
use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Create user form
 */
class UserForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $confirmPassword;
    public $status;
    public $roles;
    public $firstName;
    public $middleName;
    public $lastName;

    private $model;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => User::className(), 'filter' => function ($query) {
                if (!$this->getModel()->isNewRecord) {
                    $query->andWhere(['not', ['id' => $this->getModel()->id]]);
                }
            }],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['firstName', 'string', 'min' => 2, 'max' => 255],
            ['middleName', 'string', 'min' => 2, 'max' => 255],
            ['lastName', 'string', 'min' => 2, 'max' => 255],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::className(), 'filter' => function ($query) {
                if (!$this->getModel()->isNewRecord) {
                    $query->andWhere(['not', ['id' => $this->getModel()->id]]);
                }
            }],

            [['password', 'confirmPassword'], 'required', 'on' => 'create'],
            ['password', 'string', 'min' => 6],
            ['confirmPassword', 'compare','compareAttribute' => 'password','message' => 'Password don\'t match'],

            [['status'], 'integer'],
            [['roles'], 'each',
                'rule' => ['in', 'range' => ArrayHelper::getColumn(
                    Yii::$app->authManager->getRoles(),
                    'name'
                )]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username'        => 'Username',
            'email'           => 'Email',
            'status'          => 'Status',
            'password'        => 'Password',
            'confirmPassword' => 'Confirm Password',
            'roles'           => 'Roles',
        ];
    }

    /**
     * @param User $model
     * @return mixed
     */
    public function setModel($model)
    {
        $this->username = $model->username;
        $this->email = $model->email;
        $this->status = $model->status;
        $this->firstName = $model->firstName;
        $this->middleName = $model->middleName;
        $this->lastName = $model->lastName;
        $this->model = $model;
        $this->roles = ArrayHelper::getColumn(
            Yii::$app->authManager->getRolesByUser($model->id),
            'name'
        );

        return $this->model;
    }

    /**
     * @return User
     */
    public function getModel()
    {
        if (!$this->model) {
            $this->model = new User();
        }
        return $this->model;
    }

    /**
     * Signs user up.
     * @return User|null the saved model or null if saving fails
     * @throws Exception
     */
    public function save()
    {
        if ($this->validate()) {
            $model = $this->getModel();
            $isNewRecord = $model->getIsNewRecord();
            $model->username = $this->username;
            $model->email = $this->email;
            $model->status = $this->status;
            if ($this->password) {
                $model->setPassword($this->password);
            }
            if (!$model->save()) {
                throw new Exception('Model not saved');
            }
            if ($isNewRecord) {
                $model->afterSignup();
            }
            $auth = Yii::$app->authManager;
            $auth->revokeAll($model->getId());

            if ($this->roles && is_array($this->roles)) {
                foreach ($this->roles as $role) {
                    $auth->assign($auth->getRole($role), $model->getId());
                }
            }

            $userProfile = UserProfile::findOne(['user_id' => $model->id]);
            if (!$userProfile) {
                $userProfile = new UserProfile();
                $userProfile->user_id = $model->id;
            }
            //        Save|update user profile
            $userProfile->firstname = strip_tags($this->firstName);
            $userProfile->middlename = strip_tags($this->middleName);
            $userProfile->lastname = strip_tags($this->lastName);
            $userProfileSave = $userProfile->save();

            return !$model->hasErrors();
        }
        return null;
    }
}
