<?php
use yii\helpers\Html;

?>
<hr/>
<div class="comment-item row">
    <div class="col-xs-12">
        <div class="comment-meta">
            <span class="author">
                <?= (!empty($model->author->username)) ? $model->author->username:'Гость';?><br />
            </span>
            <span class="comment-date">
                <?php echo Yii::$app->formatter->asDatetime($model->created_at) ?>
            </span>,
        </div>
        <div class="comment-text">
            <div class="text">
                <?= $model->body ?>
            </div>
        </div>
    </div>
</div>
