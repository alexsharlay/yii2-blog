<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
?>
<div class="comments">
    <h3>Комментарии</h3>
    <?php Pjax::begin(['timeout'=>10000]); ?>
    <?php echo \yii\widgets\ListView::widget([
        'dataProvider' => $dataProvider,
        'pager'        => [
            'hideOnSinglePage' => true,
        ],
        'itemView'     => '_comment_item'
    ]) ?>
    <?php Pjax::end(); ?>


    <div class="comment-form">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

        <div class="form-group">
            <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>