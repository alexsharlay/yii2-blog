<?php

namespace frontend\components;

use frontend\models\search\PostCommentsSearch;
use Yii;
use yii\base\Widget;
use common\models\PostComments;


class CommentsWidget extends Widget
{

    public $post_id;

    public function run()
    {
        if (empty($this->post_id)) return false;

        $model = new PostComments();

        $dataProvider = $model->searchComments($this->post_id);
        $dataProvider->sort = [
            'defaultOrder' => ['created_at' => SORT_DESC],
        ];

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            $model->created_at = time();
            $model->author_id = Yii::$app->user->identity->id;
            $model->post_id = $this->post_id;
            $model->save();
        }
        return $this->render('comments', [
            'model' => new PostComments(),
            'dataProvider'=>$dataProvider,
        ]);
    }
}