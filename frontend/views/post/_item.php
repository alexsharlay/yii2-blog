<?php
/**
 * @var $this yii\web\View
 * @var $model common\models\Post
 */
use yii\helpers\Html;

?>
<hr/>
<div class="post-item row">
    <div class="col-xs-12">
        <h2 class="post-title">
            <?php echo Html::a($model->title, ['view', 'slug'=>$model->slug],['data-pjax' => 0]) ?>
        </h2>
        <div class="post-meta">
            <span class="author">
                Author: <?= $model->author->username;?><br />
            </span>
            <span class="post-date">
                <?php echo Yii::$app->formatter->asDatetime($model->created_at) ?>
            </span>,
            <span class="post-category">
                <?php echo Html::a(
                    $model->category->title,
                    ['index', 'ArticleSearch[category_id]' => $model->category_id]
                )?>
            </span>
        </div>
        <div class="post-content">
            <div class="post-text">
                <?php echo \yii\helpers\StringHelper::truncate($model->body, 150, '...', null, true) ?>
            </div>
        </div>
    </div>
</div>
