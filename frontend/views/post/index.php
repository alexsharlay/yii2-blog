<?php
use yii\widgets\Pjax;
/* @var $this yii\web\View */
?>
<div id="article-index">
    <h1>Блог</h1>
    <?php Pjax::begin(['timeout'=>2000]); ?>
    <?php echo \yii\widgets\ListView::widget([
        'dataProvider'=>$dataProvider,
        'pager'=>[
            'hideOnSinglePage'=>true,
        ],
        'itemView'=>'_item'
    ])?>
    <?php Pjax::end(); ?>
</div>