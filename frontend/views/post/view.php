<?php

use frontend\components\CommentsWidget;

/* @var $this yii\web\View */
/* @var $model common\models\Post */
$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Блог', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content">
    <article class="article-item">
        <h1><?php echo $model->title ?></h1>

        <?php echo $model->body ?>
        
        <?= CommentsWidget::widget(['post_id' =>  $model->id]); ?>
    </article>
</div>