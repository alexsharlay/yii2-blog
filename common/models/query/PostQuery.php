<?php

namespace common\models\query;
use common\models\Post;

/**
 * This is the ActiveQuery class for [[\common\models\Post]].
 *
 * @see \common\models\Post
 */
class PostQuery extends \yii\db\ActiveQuery
{
    public function published()
    {
        $this->andWhere(['status' => Post::STATUS_PUBLISHED]);
       // $this->andWhere(['<', '{{%post}}.published_at', time()]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return \common\models\Post[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Post|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
