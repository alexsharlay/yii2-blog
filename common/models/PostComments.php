<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "post_comments".
 *
 * @property integer $id
 * @property string $body
 * @property integer $post_id
 * @property integer $author_id
 * @property integer $created_at
 *
 * @property Post $post
 * @property User $author
 */
class PostComments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body'], 'required'],
            [['body'], 'string'],
            [['post_id', 'author_id', 'created_at'], 'integer'],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['post_id' => 'id']],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'body' => 'Сообщение',
            'post_id' => 'Пост',
            'author_id' => 'Автор',
            'created_at' => 'Создано',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @param $post_id
     * @return ActiveDataProvider
     */
    public function searchComments($post_id){
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize'=>5]
        ]);

        $query->andFilterWhere([
            'post_id' => $post_id,
        ]);

        return $dataProvider;
    }

    /**
     * @inheritdoc
     * @return \common\models\query\PostCommentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\PostCommentsQuery(get_called_class());
    }
}
