<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
/**
 * Инициализатор RBAC выполняется в консоли php yii rbac/init
 */
class RbacController extends Controller {

    public function actionInit() {
        $auth = Yii::$app->authManager;

        $auth->removeAll(); //На всякий случай удаляем старые данные из БД...

        // Создадим роли админа и редактора новостей
        $admin = $auth->createRole('admin');
        $editor = $auth->createRole('editor');
        $user = $auth->createRole('user');

        // запишем их в БД
        $auth->add($admin);
        $auth->add($editor);
        $auth->add($user);

        // Создаем разрешения. Например, просмотр админки viewAdminPage и редактирование новости updateNews
        $loginToBackend = $auth->createPermission('loginToBackend');
        $loginToBackend->description = 'Доступ к админке';

        $updatePosts = $auth->createPermission('updatePosts');
        $updatePosts->description = 'Редактирование новости';

        $loginToPrivateOffice = $auth->createPermission('loginToPrivateOffice');
        $loginToPrivateOffice->description = 'Доступ к личному кабинету';

        // Запишем эти разрешения в БД
        $auth->add($loginToBackend);
        $auth->add($updatePosts);
        $auth->add($loginToPrivateOffice);


        // Теперь добавим наследования. Для роли editor мы добавим разрешение updateNews,
        // а для админа добавим наследование от роли editor и еще добавим собственное разрешение viewAdminPage

        //Вход в личный кабинет для пользователя
        $auth->addChild($user, $loginToPrivateOffice);

        // Роли «Редактор новостей» присваиваем разрешение «Редактирование новости»
        //И наследуем от роли пользователь
        $auth->addChild($editor, $user);
        $auth->addChild($editor,$updatePosts);

        // админ наследует роль редактора новостей. Он же админ, должен уметь всё! :D
        $auth->addChild($admin, $editor);

        // Еще админ имеет собственное разрешение - «Просмотр админки»
        $auth->addChild($admin, $loginToBackend);

        // Назначаем роль admin пользователю с ID 1
        $auth->assign($admin, 1);

        // Назначаем роль editor пользователю с ID 2
        $auth->assign($editor, 2);

        // Назначаем роль editor пользователю с ID 3
        $auth->assign($user, 3);
    }
}