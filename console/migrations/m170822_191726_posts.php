<?php

use yii\db\Migration;

class m170822_191726_posts extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%post_category}}', [
            'id'         => $this->primaryKey(),
            'title'      => $this->string(512)->notNull(),
            'slug'       => $this->string(1024)->notNull(),
            'status'     => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->createTable('{{%post}}', [
            'id'                 => $this->primaryKey(),
            'title'              => $this->string(512)->notNull(),
            'slug'               => $this->string(1024)->notNull(),
            'body'               => $this->text()->notNull(),
            'category_id'        => $this->integer(),
            'author_id'          => $this->integer(),
            'updater_id'         => $this->integer(),
            'status'             => $this->smallInteger()->notNull()->defaultValue(0),
            'published_at' => $this->integer(),
            'created_at'         => $this->integer(),
            'updated_at'         => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_post_author', '{{%post}}', 'author_id', '{{%user}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_post_updater', '{{%post}}', 'updater_id', '{{%user}}', 'id', 'set null', 'cascade');
        $this->addForeignKey('fk_post_category', '{{%post}}', 'category_id', '{{%post_category}}', 'id', 'cascade', 'cascade');
    }

    public function down()
    {
        $this->dropForeignKey('fk_post_author', '{{%post}}');
        $this->dropForeignKey('fk_post_updater', '{{%post}}');
        $this->dropForeignKey('fk_post_category', '{{%post}}');

        $this->dropTable('{{%post}}');
        $this->dropTable('{{%post_category}}');
    }
}
