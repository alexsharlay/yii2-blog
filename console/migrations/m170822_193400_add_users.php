<?php

use yii\db\Migration;
use common\models\User;
class m170822_193400_add_users extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%user}}', [
            'id'            => 1,
            'username'      => 'admin',
            'email'         => 'admin@blog.com',
            'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('admin'),
            'auth_key'      => Yii::$app->getSecurity()->generateRandomString(),
            'status'        => User::STATUS_ACTIVE,
            'created_at'    => time(),
            'updated_at'    => time()
        ]);
        $this->insert('{{%user}}', [
            'id'            => 2,
            'username'      => 'editor',
            'email'         => 'editor@blog.com',
            'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('editor'),
            'auth_key'      => Yii::$app->getSecurity()->generateRandomString(),
            'status'        => User::STATUS_ACTIVE,
            'created_at'    => time(),
            'updated_at'    => time()
        ]);
        $this->insert('{{%user}}', [
            'id'            => 3,
            'username'      => 'user',
            'email'         => 'user@blog.com',
            'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('user'),
            'auth_key'      => Yii::$app->getSecurity()->generateRandomString(),
            'status'        => User::STATUS_ACTIVE,
            'created_at'    => time(),
            'updated_at'    => time()
        ]);

        $this->insert('{{%user_profile}}', [
            'user_id'   => 1,
            'firstname' => 'John',
            'lastname'  => 'Doe'
        ]);
        $this->insert('{{%user_profile}}', [
            'user_id' => 2,
        ]);
        $this->insert('{{%user_profile}}', [
            'user_id' => 3,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%user_profile}}', [
            'user_id' => [1, 2, 3]
        ]);

        $this->delete('{{%user}}', [
            'id' => [1, 2, 3]
        ]);
    }
}
