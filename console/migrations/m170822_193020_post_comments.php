<?php

use yii\db\Migration;

class m170822_193020_post_comments extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%post_comments}}', [
            'id'         => $this->primaryKey(),
            'body'       => $this->text()->notNull(),
            'post_id'    => $this->integer(),
            'author_id'  => $this->integer(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_comments_author', '{{%post_comments}}', 'author_id', '{{%user}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_comments_post', '{{%post_comments}}', 'post_id', '{{%post}}', 'id', 'cascade', 'cascade');

    }

    public function down()
    {
        $this->dropForeignKey('fk_comments_author', '{{%post_comments}}');
        $this->dropForeignKey('fk_comments_post', '{{%post_comments}}');

        $this->dropTable('{{%post_comments}}');
    }
}
